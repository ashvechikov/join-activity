<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<%@ page session="true" %>
<html>
<head>
    <title><spring:message code="page.title.login"/></title>
    <meta name="keywords" content="<spring:message code='page.keywords'/>">
    <meta name="description" content="<spring:message code='page.description'/>">
</head>
<body>
<div class="container login-container">
    <div class="login-block">
        <div>
            <h2><spring:message code="page.login.enter"/></h2>
            <a id="password-request" href="#" class="grey forgot-link"><spring:message code="page.login.forgot.password"/></a>
            <div class="password-request-div">

            </div>
            <div class="clear"></div>
        </div>
        <form action="/j_spring_security_check" class="form-horizontal" method='POST'>
            <div>
                <input type="email" class="inputEmail" name='j_username' autofocus="autofocus" value="${param.email}" placeholder="<spring:message code='page.login.email' />">
            </div>
            <div>
                <input type="password" class="inputPass" placeholder="<spring:message code='page.login.password' />" name='j_password'>
            </div>
            <c:if test="${not empty param.error && param.error}">
                <div class="text-error left-align"><spring:message code="AbstractUserDetailsAuthenticationProvider.badCredentials" arguments="${SPRING_SECURITY_LAST_EXCEPTION.message}"/></div>
            </c:if>
            <div class="buttons">
                <div class="checkbox">
                    <input type="checkbox" id="checkbox1" name='_spring_security_remember_me'> <label
                        for="checkbox1"><spring:message code="page.login.remember.me"/></label>
                </div>
                <button type="submit" class="btn-enter pull-right"><spring:message code="page.login.enter"/></button>
               <div class="clear"></div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>
</body>
</html>