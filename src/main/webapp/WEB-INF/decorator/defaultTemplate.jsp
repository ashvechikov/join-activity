<%@ include file="/WEB-INF/include/pageHead.jsp"%>

<!DOCTYPE html>
<html>
<head>
    <link rel="icon" href="/resources/images/favicon.ico" type="image/x-icon">

    <link href="${pageContext.request.contextPath}/resources/css/styles.css" type="text/css" rel="stylesheet" />
    <script src="/resources/js/jquery-1.9.1.js"></script>

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
</head>
<body>
    <sec:authorize access="isAuthenticated()" var="loggedIn" />

    <div class="header">
        Some header
        <c:choose>
            <c:when test="${loggedIn}">
                <span style="margin: 10px; color: blue">Logged</span>
                <a href="/j_spring_security_logout">Logout</a>
            </c:when>
            <c:otherwise>
                <a href="/register">Register</a>
                <a href="/login">Login</a>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="navigation">
        <ul>
            <li>
                <spring:message code="navigation.home" var="home"/>
                <ja:urlHandler url="/" text="${home}"/>
            </li>
        </ul>
    </div>

    <sitemesh:write property='body'/>

    <div class="footer">
        <spring:message code="copyright"/>
    </div>
</body>
</html>