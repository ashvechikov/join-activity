package com.joinactivity.core.service.cache;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface CacheProvider {

    void set(String key, int exp, Object o);
    void delete(String key);
    Object get(String key);
    <T> T get(String key, Class<T> clazz);
    <T> List<T> getAsList(String key, Class<T> clazz);

    <T> Map<String, T> get(Collection<String> keys, Class<T> clazz);
}
