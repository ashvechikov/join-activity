package com.joinactivity.core.service.cache;

import java.util.ArrayList;
import java.util.List;

public class CacheKeys {

    public static final String POPULAR_CATEGORY_GROUPS = "popular.category.groups";
    public static final String ALL_CATEGORY_GROUPS = "all.category.groups";
    public static final String CATEGORY_GROUP = "category.group.%s";

    public static final String LAST_NEWS_KEY = "last.news";
    public static final String NEWS_ITEM_KEY = "news.item.%s";

    public static final String CONSTANT = "constant.%s";
    public static final String ALL_CONSTANTS = "all.constants";

    public static final String UNREAD_MSG_COUNT = "unread.msg.count.%s";

    public static final String MAX_RATING = "max.rating";
    public static final String MIN_RATING = "min.rating";

    public static String getKey(String format, Object args) {
        return String.format(format, args);
    }

    public static String getUnreadMsgCountKey(String id) {
        return getKey(UNREAD_MSG_COUNT, id);
    }

    public static String getCategoryGroupKey(String id) {
        return getKey(CATEGORY_GROUP, id);
    }

    public static String getConstantRatingKey(String id) {
        return getKey(CONSTANT, id);
    }

    public static List<String> getConstantRatingKeys(List<String> ids) {
        return getListKeys(ids, CONSTANT);
    }

    public static String getNewsItemKey(String id) {
        return getKey(NEWS_ITEM_KEY, id);
    }

    public static List<String> getCategoryGroupKeys(List<String> ids) {
        return getListKeys(ids, CATEGORY_GROUP);
    }

    public static List<String> getNewsKeys(List<String> ids) {
        return getListKeys(ids, NEWS_ITEM_KEY);
    }

    /*
     * Get list of keys corresponding to format
     */
    private static List<String> getListKeys(List<String> ids, String format) {
        List<String> keys = new ArrayList<>(ids.size());
        for (String id : ids) {
            keys.add(String.format(format, id));
        }
        return keys;
    }
}
