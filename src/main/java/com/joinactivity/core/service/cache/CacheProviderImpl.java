package com.joinactivity.core.service.cache;

import lombok.extern.slf4j.Slf4j;
import net.rubyeye.xmemcached.MemcachedClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CacheProviderImpl implements CacheProvider {

    @Autowired
    private MemcachedClient client;

    @Override
    public void set(String key, int exp, Object o) {
        try {
            if(o != null) {
                client.set(key, exp, o);
            }
        } catch (Exception e) {
            log.error("Ошибка сохранения в мемкеш", e);
        }
    }

    @Override
    public void delete(String key) {
        try {
            client.delete(key);
        } catch (Exception e) {
            log.error("Ошибка удаления из мемкеша", e);
        }
    }

    @Override
    public Object get(String key) {
        try {
            return client.get(key, 1000);
        } catch (Exception e) {
            log.error("Ошибка получения из кеша", e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
	@Override
    public <T> T get(String key, Class<T> clazz) {
        return (T) get(key);
    }

    @SuppressWarnings("unchecked")
	@Override
    public <T> List<T> getAsList(String key, Class<T> clazz) {
        return (List<T>) get(key, clazz);
    }

    @SuppressWarnings("unchecked")
	@Override
    public <T> Map<String, T> get(Collection<String> keys, Class<T> clazz) {
        try {
            Map<String, Object> result = client.get(keys, 1000);
            Map<String, T> map = new HashMap<>(result.size());
            for (Map.Entry<String, Object> entry : result.entrySet()) {
                map.put(entry.getKey(), (T) entry.getValue());
            }
            return map;
        } catch (Exception e) {
            log.error("Ошибка получения по списку ключей", e);
            return new HashMap<String, T>(0);
        }
    }
}
