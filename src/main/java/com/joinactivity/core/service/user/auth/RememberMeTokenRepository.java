package com.joinactivity.core.service.user.auth;

import com.joinactivity.core.model.user.auth.RememberMeToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RememberMeTokenRepository extends MongoRepository<RememberMeToken, String>{

    RememberMeToken findBySeries(String series);
    List<RememberMeToken> findByUsername(String username);

}
