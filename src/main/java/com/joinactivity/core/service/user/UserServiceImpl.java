package com.joinactivity.core.service.user;

import com.google.common.collect.Lists;
import com.joinactivity.core.model.user.User;
import com.joinactivity.core.model.user.UserRole;
import com.joinactivity.core.model.user.register.RegistrationData;
import com.joinactivity.core.service.cache.CacheProvider;
import com.joinactivity.utils.Patterns;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.util.Assert.notNull;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SaltSource saltSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private CacheProvider cacheProvider;

    @Override
    public User register(User user) {
        notNull(user);
        user.setUserRoles(Lists.newArrayList(UserRole.ROLE_USER));
        user.setEnabled(true);
        user.setRegistrationDate(new Date());
        user = userRepository.save(user);
        return user;
    }

    @Override
    public User register(RegistrationData registrationData) {
        notNull(registrationData);
        User user = new User();
        BeanUtils.copyProperties(registrationData, user);
        String cryptedPassword = passwordEncoder.encodePassword(registrationData.getPassword(), saltSource.getSalt(user));
        user.setPassword(cryptedPassword);
        user.setUserRoles(Lists.newArrayList(UserRole.ROLE_USER));
        user.setEnabled(true);
        user = userRepository.save(user);
        sendEmailConfirm(user);
        return user;
    }

    @Override
    public void sendEmailConfirm(User user) {
        user.setEmailConfirmKey(UUID.randomUUID().toString());
        user.setEmailConfirmed(false);
//        emailService.sendEmailConfirm(user);
        save(user);
    }
    @Override
    public User save(User user) {
        notNull(user);
        return userRepository.save(user);
    }

    @Override
    public User findOne(String id) {
        if(StringUtils.isEmpty(id) || !ObjectId.isValid(id)) {
            return null;
        }
        return userRepository.findOne(id);
    }

    @Override
    public User findByEmail(String email){
        if(StringUtils.isEmpty(email) || !Pattern.matches(Patterns.EMAIL, email)){
            return null;
        }
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByEmailConfirmKey(String key) {
        if(StringUtils.isEmpty(key)) {
            return null;
        }
        return userRepository.findByEmailConfirmKey(key);
    }

    @Override
    public void sendPasswordRecoveryKey(User user) {
        user.setPasswordRecoveryKey(UUID.randomUUID().toString());
//        emailService.sendPasswordRecovery(user);
        save(user);
    }
    @Override
    public User findByPasswordRecoveryKey(String key) {
        if(StringUtils.isEmpty(key)) {
            return null;
        }
        return userRepository.findByPasswordRecoveryKey(key);
    }

    @Override
    public List<User> findByEmailConfirmed(boolean emailConfirmed) {
        return userRepository.findByEmailConfirmed(emailConfirmed);
    }

    private Page<User> findByCriteria(Criteria criteria, Pageable pageable){
        Query query = Query.query(criteria).with(pageable);
        List<User> result = mongoTemplate.find(query, User.class);
        long total = mongoTemplate.count(query, User.class);
        return new PageImpl<>(result, pageable, total);
    }

    @Override
    public User setAvatar(User user) {
        notNull(user);
        if(!user.isAvatar()) {
            user.setAvatar(true);
            user = save(user);
        }
        return user;
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public long countByRegistrationDate(Date startDate) {
        return mongoTemplate.count(Query.query(where("registrationDate").gte(startDate)), User.class);
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }
}
