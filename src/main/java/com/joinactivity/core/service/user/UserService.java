package com.joinactivity.core.service.user;

import com.joinactivity.core.model.user.User;
import com.joinactivity.core.model.user.register.RegistrationData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;

public interface UserService {

    User findByEmail(String email);

    User save(User user);

    User findOne(String id);

	User findByEmailConfirmKey(String key);

	void sendEmailConfirm(User user);

	void sendPasswordRecoveryKey(User user);

	User findByPasswordRecoveryKey(String key);

    User setAvatar(User user);

    List<User> findByEmailConfirmed(boolean emailConfirmed);

    User register(User user);
    User register(RegistrationData registrationData);
    Page<User> findAll(Pageable pageable);

    long countByRegistrationDate(Date startDate);
    long count();

    Iterable<User> findAll();
}
