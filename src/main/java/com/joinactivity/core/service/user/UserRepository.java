package com.joinactivity.core.service.user;

import com.joinactivity.core.model.user.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    User findByEmail(String email);

	User findByEmailConfirmKey(String key);

	User findByPasswordRecoveryKey(String key);

//    @Query("{categories.$id:?0}")
//    Page<User> findByCategory(ObjectId categoryId, Pageable pageable);
//
//    @Query("{categories.$id:?0, userInfo.location.locationName:?1}")
//    Page<User> findByCategoryAndLocation(ObjectId categoryId, String location, Pageable pageable);

    List<User> findByEmailConfirmed(boolean emailConfirmed);
}
