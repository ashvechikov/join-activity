package com.joinactivity.core.model.user.info;

import com.joinactivity.utils.Patterns;
import com.joinactivity.utils.WordUtils;
import lombok.Data;
import lombok.Delegate;
import org.joda.time.DateTime;
import org.joda.time.Years;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Data
public class UserInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    @Past
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthday;

    @Valid
    @Delegate

    @Size(min = 0, max = 500)
    private String description;

    @Size(min = 0, max = 100)
    private String status;

    @NotNull
    @Size(min = 2, max = 20)
    @Pattern(regexp = Patterns.NAME)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20)
    @Pattern(regexp = Patterns.NAME)
    private String lastName;

    public int getAge(){
        return Years.yearsBetween(new DateTime(birthday), DateTime.now()).getYears();
    }

    public String getFullName() {
        return WordUtils.escapeText(String.format("%s %s", firstName, lastName));
    }
    public String getStatus() {
    	return WordUtils.escapeText(status);
    }
}
