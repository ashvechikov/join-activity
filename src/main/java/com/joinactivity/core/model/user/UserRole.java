package com.joinactivity.core.model.user;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN;
}
