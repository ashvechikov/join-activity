package com.joinactivity.core.model.user.register;

import com.google.common.base.Objects;
import com.joinactivity.utils.Patterns;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class RegistrationData implements Serializable {
	private static final long serialVersionUID = 1L;

    @NotNull
    @Pattern(regexp = Patterns.EMAIL)
    @Indexed(unique = true)
    private String email;

    @NotNull
    @Pattern(regexp = Patterns.NAME)
    private String firstName;

    @NotNull
    @Pattern(regexp = Patterns.NAME)
    private String lastName;

    @NotNull
    @Pattern(regexp = Patterns.PASSWORD)
    private String password;

    private String rePassword;

    private Date registrationDate = new Date();

    @AssertTrue
    public boolean isPasswordsEquals(){
        if(password == null){
            return true;
        }
        return password.equals(rePassword);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RegistrationData)) return false;

        RegistrationData data = (RegistrationData) o;
        return  Objects.equal(email, data.getEmail()) &&
                Objects.equal(firstName, data.getFirstName()) &&
                Objects.equal(lastName, data.getLastName()) &&
                Objects.equal(password, data.getPassword()) &&
                Objects.equal(rePassword, data.getRePassword());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(email, firstName, lastName, password, rePassword);
    }
}
