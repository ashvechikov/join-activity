package com.joinactivity.core.model.user;

import com.google.common.base.Objects;
import com.joinactivity.core.model.user.info.UserInfo;
import com.joinactivity.utils.Patterns;
import com.joinactivity.utils.WordUtils;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.*;

@Data
public class User implements Serializable, UserDetails {
    private static final long serialVersionUID = 1L;

	@Id
    private String id;

    @Pattern(regexp = Patterns.EMAIL)
    private String email;
    private String emailConfirmKey;
    private boolean emailConfirmed;
    private String passwordRecoveryKey;

    private String password;

    @NotNull
    @Past
    private Date registrationDate;

    private boolean avatar;

    @NotNull
    @NotEmpty
    private List<UserRole> userRoles;

    private boolean enabled;

    private int rating = 0;

    private boolean employeeProfileActivated;

    private UserInfo userInfo = new UserInfo();

    public User() {}

    public String getProfileUri(){
        return "/user/" + id;
    }
    
    public String getAvatarUri(){
    	return "/media"+ getProfileUri() +"/avatar";
    }

//    public String getAvatarUri(@NotNull ImageSize imageSize){
//    	return getAvatarUri() +"/" + imageSize.getSize();
//    }

    public String getFirstName(){
        return userInfo.getFirstName();
    }

    public String getLastName(){
        return userInfo.getLastName();
    }

    public void setFirstName(String firstName){
        userInfo.setFirstName(firstName);
    }

    public void setLastName(String lastName){
        userInfo.setLastName(lastName);
    }

    public String getFullName() {
        return WordUtils.escapeText(String.format("%s %s", userInfo.getFirstName(), userInfo.getLastName()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>(userRoles.size());
        for (UserRole userRole : userRoles) {
            authorities.add(new SimpleGrantedAuthority(userRole.toString()));
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o){
        if(o == null){
            return false;
        }
        if(this.getClass() != o.getClass()){
            return false;
        }
        User u = (User) o;
        return  Objects.equal(email, u.getEmail()) &&
                Objects.equal(userInfo.getFirstName(), u.getFirstName()) &&
                Objects.equal(userInfo.getLastName(), u.getLastName());
    }

    @Override
    public int hashCode(){
        return com.google.common.base.Objects.hashCode(email, userInfo.getFirstName(), userInfo.getLastName());
    }
}
