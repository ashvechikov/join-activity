package com.joinactivity.utils;

public class NumberEndingsRussian {
	/* Беглые гласные, чередование и прочее не учитываются */

	// сообщений
	public static String getMultipleEnding(long c){
		return getCertainEnding(c, null, "е", "я", "й");
	}

	// ответ
	public static String getMaleEnding(long c){
		return getCertainEnding(c, null, "", "а", "ов");
	}

	// день
	public static String declineMale(String wordFull, String wordShort, long c){
		if (c % 10 == 1 && c / 10 != 1) {
			return Long.toString(c) + " " + wordFull;
		}
		return Long.toString(c) + " " + wordShort + getCertainEnding(c, null, "", "я", "ей");
	}

	// месяц
	public static String getMale2Ending(long c){
		return getCertainEnding(c, null, "", "а", "ев");
	}
	
//	public static String getWordWithEnding(long count, String mnemoPrefix, String lang){
//		LocalizationManager localization = LocalizationManager.getInstance();
//		if(count % 100 == 11){
//			return localization.getPhrase(lang, mnemoPrefix + ".other");
//		}
//		long lastDigit = count % 10;
//		if(lastDigit == 1){
//			return localization.getPhrase(lang, mnemoPrefix + ".1");
//		}
//		if((lastDigit >=2 && lastDigit <= 4)){
//			return localization.getPhrase(lang, mnemoPrefix + ".2.4");
//		}
//		return localization.getPhrase(lang, mnemoPrefix + ".other");
//	}

	// раз
	public static String getMaleShortEnding(long c){
		return getCertainEnding(c, null, "", "а", "");
	}

	// тема
	public static String getFemaleStrongEnding(long c){
		return getCertainEnding(c, null, "а", "ы", "");
	}

	// елка
	public static String getFemaleNormalEnding(long c){
		return getCertainEnding(c, null, "ка", "ки", "ок");
	}

	// цепь
	public static String getFemaleSoftEnding(long c){
		return getCertainEnding(c, null, "ь", "и", "ей");
	}
	
	//фотография
	public static String getFemaleMultiplyEnding(long count) {
		return getCertainEnding(count, null, "я", "и", "й");
	}
	
	public static String getAdjective(int count) {
		return getCertainEnding(count, null, "ое", "ых", "ых");
	}
	
	/**Общий метод, чтобы не повторяться не повторяться не повторяться...*/
	public static String getCertainEnding(long c, String wordWithoutEnding, String end1, String end2_4, String endOther) {
		long v = c % 100;
		String resultEnding = "";
		if ((v / 10 == 0) || (v / 10 >= 2)) {
			long v_rem_10 = v % 10;
			if (v_rem_10 == 1) resultEnding = end1;
			else if ((v_rem_10 >= 2) && (v_rem_10 <= 4)) resultEnding = end2_4;
			else resultEnding = endOther;
		} else resultEnding = endOther;
		return wordWithoutEnding == null ? resultEnding : wordWithoutEnding + resultEnding;
	}
	
	/**Возвращает строку окончания, перед которой стоит число*/
	public static String getCertainEndingWithNumber(long c, String worldWithoutEnding, String end1, String end2_4, String endOther) {
		return (worldWithoutEnding != null) 
				? new StringBuilder().append(c).append(" ").append(getCertainEnding(c, worldWithoutEnding, end1, end2_4, endOther)).toString()
						: "";
	}
}
