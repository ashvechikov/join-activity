package com.joinactivity.utils.ajax;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.Data;

import java.util.Collection;

@Data
public class JsonResponse {

    private JsonResponseStatus status;
    private Object result;
    private JsonElement json;

    public JsonResponse(){}

    public JsonResponse(JsonResponseStatus status){
        setStatus(status);
    }

    public JsonResponse(JsonResponseStatus status, Object result){
        setStatus(status);
        setResult(result);
    }

    public JsonResponse(JsonResponseStatus status, String key, String value) {
        setStatus(status);
        JsonObject json = new JsonObject();
        json.addProperty(key, value);
        setJson(json);
    }
    
    public JsonResponse(JsonElement result) {
        setStatus(JsonResponseStatus.SUCCESS);
        this.json = result;
    }
    
    public JsonResponse(Collection<String> errorMessages){
        setStatus(JsonResponseStatus.ERROR);
        setResult(errorMessages);
    }
    public String toString() {
    	JsonObject json = new JsonObject();
    	json.addProperty("status", status.name());
    	json.add("result", this.json);
    	return json.toString();
    }
}
