package com.joinactivity.web.tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.support.JspAwareRequestContext;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Locale;

public abstract class AbstractSpringTag extends SimpleTagSupport {

    @Autowired
    protected MessageSource messageSource;

    public static final String REQUEST_CONTEXT_PAGE_ATTRIBUTE = "org.springframework.web.servlet.tags.REQUEST_CONTEXT";

    protected PageContext pageContext;
    protected RequestContext requestContext;
    protected HttpServletRequest request;


    @Override
    public void doTag() throws JspException, IOException {
        this.pageContext = (PageContext) getJspContext();
        this.requestContext = (RequestContext)pageContext.getAttribute(REQUEST_CONTEXT_PAGE_ATTRIBUTE);
        if (this.requestContext == null) {
            this.requestContext = new JspAwareRequestContext(this.pageContext);
            this.pageContext.setAttribute(REQUEST_CONTEXT_PAGE_ATTRIBUTE, this.requestContext);
        }
        messageSource = requestContext.getWebApplicationContext().getBean(MessageSource.class);
        this.request = (HttpServletRequest) pageContext.getRequest();
        write(getJspContext().getOut());
    }

    protected Locale getLocale() {
        return requestContext.getLocale();
    }

    public String getMessage(String code, Object[] objects){
        try{
            return messageSource.getMessage(code, objects, getLocale());
        } catch (Exception e){
            return code;
        }
    }

    public abstract void write(JspWriter out) throws IOException, JspException;

}
