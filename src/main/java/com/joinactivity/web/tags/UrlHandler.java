package com.joinactivity.web.tags;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import java.io.IOException;

public class UrlHandler extends AbstractSpringTag {

    private String url;
    private String text;

    public static final String JAVAX_SERVLET_FORWARD_REQUEST_URI = "javax.servlet.forward.request_uri";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void write(JspWriter out) throws IOException {
        if (StringUtils.isNotEmpty(url)) {
            HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
            String pageUrl = (String) request.getAttribute(JAVAX_SERVLET_FORWARD_REQUEST_URI);
            if (pageUrl.equals(url)) {
                out.print(String.format("<p class='currentUrl'>" + text + "</p>"));
            } else {
                out.print(String.format("<a href='" + url + "'>" + text + "</a>"));
            }
        }
        out.print(String.format(""));
    }
}