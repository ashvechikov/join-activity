package com.joinactivity.web.controller;

import com.joinactivity.core.model.user.User;
import com.joinactivity.core.service.user.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Locale;

public class AbstractController {

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    private UserService userService;

    public String getMessage(String code) {
        return getMessage(code, null);
    }

    public String getMessage(String code, Object[] objects){
        try{
            return messageSource.getMessage(code, objects, getLocale());
        } catch (Exception e){
            return code;
        }
    }

    protected Locale getLocale() {
        return RequestContextUtils.getLocale(request);
    }

    public Cookie getCookie(String name){
        Cookie[] cookies = request.getCookies();
        if(cookies == null || StringUtils.isEmpty(name)){
            return null;
        }
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals(name)){
                return cookie;
            }
        }
        return null;
    }

    public boolean isAutheficated(){
        return getUser() != null;
    }

    public User getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth == null || auth.getPrincipal() == null){
            return null;
        }
        return userService.findByEmail(auth.getName());
    }

    protected void doAutoLogin(String username, String password, Collection<? extends GrantedAuthority> roles) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(username, password, roles));

        // Create a new session and add the security context.
        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
    }
}
