package com.joinactivity.web.controller;

import com.joinactivity.core.model.user.User;
import com.joinactivity.core.model.user.register.RegistrationData;
import com.joinactivity.core.service.user.UserService;
import com.joinactivity.web.messages.SuccessMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class RegisterController extends AbstractController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registerPageRequest(){
        if(isAutheficated()){
            return new ModelAndView("redirect:/");
        }
        return getRegisterModelAndView(new RegistrationData());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ModelAndView register(@Valid RegistrationData registrationData, BindingResult bindingResult, final RedirectAttributes redirectAttributes){
        if(bindingResult.hasErrors()){
            return getRegisterModelAndView(registrationData);
        }
        User user = userService.register(registrationData);
        doAutoLogin(registrationData.getEmail(), registrationData.getPassword(), user.getAuthorities());
        redirectAttributes.addFlashAttribute("alert", new SuccessMessage("registration.success"));
        return new ModelAndView("redirect:/");
    }

    private ModelAndView getRegisterModelAndView(RegistrationData data) {
        ModelAndView modelAndView = new ModelAndView("register", "registrationData", data);
//        modelAndView.addObject("networks", socialNetworkService.getSocialNetworks());
        return modelAndView;
    }

}
