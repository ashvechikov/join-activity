package com.joinactivity.web.controller;

import com.joinactivity.core.model.user.User;
import com.joinactivity.core.service.user.UserService;
import com.joinactivity.utils.Patterns;
import com.joinactivity.utils.ajax.JsonResponse;
import com.joinactivity.utils.ajax.JsonResponseStatus;
import com.joinactivity.web.messages.ErrorMessage;
import com.joinactivity.web.messages.SuccessMessage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.regex.Pattern;

@Controller
public class LoginController extends AbstractController {

    private static final String LOGIN_VIEW =  "login";

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SaltSource saltSource;

    @Autowired
    private UserService userService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		return new ModelAndView(LOGIN_VIEW);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout() {
		return new ModelAndView(LOGIN_VIEW);
	}

	@RequestMapping(value = "/login/ajax/password-request", method = RequestMethod.GET)
	public ModelAndView passwordRequest() {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		return new ModelAndView("password-request");
	}

    @RequestMapping(value = "/password-request", method = RequestMethod.POST)
    public @ResponseBody
    JsonResponse passwordRequest(String userEmail){
        User user = userService.findByEmail(userEmail);
        if(user == null || !user.isEnabled()) {
            return new JsonResponse(JsonResponseStatus.ERROR, getMessage("user.not.found"));
        }
        userService.sendPasswordRecoveryKey(user);
        return new JsonResponse(JsonResponseStatus.SUCCESS, getMessage("password.request.send.success"));
    }

	@RequestMapping(value = "/password-recovery/{key}", method = RequestMethod.GET)
	public ModelAndView passwordRecovery(@PathVariable String key, final RedirectAttributes redirectAttributes) {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		User user = userService.findByPasswordRecoveryKey(key);
		boolean error = false;
		if (user == null) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("password.recovery.key.not.found"));
			error = true;
		} else if (!user.isEnabled()) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("access.denied"));
			error = true;
		}
		if (error) {
			return new ModelAndView("redirect:/");
		}
		return new ModelAndView("password-recovery");
	}

	@RequestMapping(value = "/password-recovery/{key}", method = RequestMethod.POST)
	public ModelAndView passwordRecovery(@PathVariable String key, @RequestParam String password, @RequestParam String password2, final RedirectAttributes redirectAttributes) {
		if (isAutheficated()) {
			return new ModelAndView("redirect:/");
		}
		User user = userService.findByPasswordRecoveryKey(key);
		boolean error = false;
		if (user == null) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("password.recovery.key.not.found"));
			error = true;
		} else if (!user.isEnabled()) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("access.denied"));
			error = true;
		}
		if (error) {
			return new ModelAndView("redirect:/");
		}
		if(StringUtils.isEmpty(password) || StringUtils.isEmpty(password2)) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("NotNull.registrationData.password"));
			error = true;
		} else if(!password.equals(password2)) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("AssertTrue.registrationData.passwordsEquals"));
			error = true;
		} else if(isPasswordNotValid(password)) {
			redirectAttributes.addFlashAttribute("alert", new ErrorMessage("Pattern.registrationData.password"));
			error = true;
		}
		if(error) {
			return new ModelAndView("redirect:/password-recovery/"+key);
		}
		String cryptedPassword = passwordEncoder.encodePassword(password, saltSource.getSalt(user));
		user.setPassword(cryptedPassword);
		userService.save(user);
//		emailService.sendPasswordChanged(user);
		redirectAttributes.addFlashAttribute("alert", new SuccessMessage("password.change.success"));
		doAutoLogin(user.getEmail(), password, user.getAuthorities());
		return new ModelAndView("redirect:/");
	}

	private boolean isPasswordNotValid(String password) {
		return !Pattern.matches(Patterns.PASSWORD, password);
	}
}
