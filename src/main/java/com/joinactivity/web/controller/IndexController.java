package com.joinactivity.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class IndexController extends AbstractController {

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView load() {
        return new ModelAndView("index", "message", "Some message");
    }
}
